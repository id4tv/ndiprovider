﻿using System;
using Backend.Network.ProtocolSdk;
using Lib.AVFormat;
using Lib.Log;
using NewTek;
using NewTek.NDI;

namespace Lib.NDI
{
    public class SendManager : IDisposable
    {
        private NDIlib.video_frame_v2_t _frame;
        private string _ndiName;
        private Sender _sender;

        public void Send(IntPtr source)
        {
            if (source == IntPtr.Zero)
            {
                string msg = $"{_ndiName}: the data is empty, will not send to NDI channel.";
                Logger.Info("NDIEngine.log", msg);
                return;
            }

            // are we connected to anyone?
            if (_sender.Connections < 1)
            {
                // no point rendering
                string msg = $"{_ndiName}: No current connections, so no rendering needed.";
                Logger.Info("NDIEngine.log", msg);
            }
            else
            {
                _frame.p_data = source;
                _sender.Send(ref _frame);
            }
        }

        public void Init(string ndiName, FormatInfo formatInfo)
        {
            _ndiName = ndiName;
            string mes = $"Init NDI sender: {ndiName},Width: {formatInfo.Width}, Height: {formatInfo.Height},Num: {formatInfo.Num},Den: {formatInfo.Den}.";
            // .Net interop doesn't handle UTF-8 strings, so do it manually
            // These must be freed later
            _sender = new Sender(ndiName);
            Logger.Info("NDIEngine.log", mes);
            int stride = formatInfo.Width * 4; //(xres * 32/*BGRA bpp*/ + 7) / 8;
            _frame = new NDIlib.video_frame_v2_t
            {
                // Resolution
                xres = formatInfo.Width,
                yres = formatInfo.Height,
                // Use BGRA video
                FourCC = NDIlib.FourCC_type_e.FourCC_type_BGRA,
                // The frame-eate
                frame_rate_N = (int)formatInfo.Num,
                frame_rate_D = (int)formatInfo.Den,
                // The aspect ratio (16:9)
                picture_aspect_ratio = (16.0f / 9.0f),
                // This is a progressive frame
                frame_format_type = formatInfo.ServerFrequency == ServerFrequencyEnum.Interlaced ? NDIlib.frame_format_type_e.frame_format_type_interleaved : NDIlib.frame_format_type_e.frame_format_type_progressive,
                // Timecode.
                timecode = NDIlib.send_timecode_synthesize,
                // The video memory used for this frame
                p_data = IntPtr.Zero,
                // The line to line stride of this image
                line_stride_in_bytes = stride,
                // no metadata
                p_metadata = IntPtr.Zero,
                // only valid on received frames
                timestamp = 0
            };
            Logger.Info("NDIEngine.log", $"Init NDI sender finished!!!  {_ndiName}");
        }

        public void Dispose()
        {
            _sender?.Dispose();
            _sender = null;
            _ndiName = null;
        }
    }
}
