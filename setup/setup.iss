#define AppWindowName "NDIProvider"
#define AppExeName "NDIProvider.exe"
#define AppExePath "..\Release\NDIProvider.exe"
#define AppVersion GetVersionNumbersString(AppExePath)
#define AppID "28ac37db-12aa-4f8f-9340-397095e62ac1"
#define ConfigPath "C:\ProgramData\SimplyLive.TV\NDIProvider\"
#define VCLStyle "Glossy.vsf"
;net runtime
#define Componentnetnet "net_runtime_x64"
#define netruntimename "Install Microsoft .net Runtime ..."
#define Dotnetruntime "dotnet-runtime-9.0.0-win-x64.exe"
#define Aspnetcoreruntime "aspnetcore-runtime-9.0.0-win-x64.exe"

[Setup]
AppId                    = {#AppID}
AppVerName               = NDIProvider {#AppVersion}
AppName                  = NDIProvider
AppVersion               = {#AppVersion}
VersionInfoVersion       = {#AppVersion}
VersionInfoCopyright     = 2021(C)SIMPLYLIVE.TV All rights reserved.
DefaultDirName           = {pf64}\SIMPLYLIVE.TV\NDIProvider\
Compression              = lzma2/ultra
SolidCompression         = Yes
PrivilegesRequired       = admin
WizardImageFile          = wizard.bmp
WizardSmallImageFile     = small wizard.bmp
OutputBaseFilename       = NDIProvider_{#AppVersion}
SetupIconFile            = NDIProvider.ico
TimeStampsInUTC          = True
DefaultGroupName         = "NDIProvider"
DisableProgramGroupPage  = True
DisableStartupPrompt     = Yes
DisableWelcomePage       = False
DisableReadyPage         = False
DisableReadyMemo         = False
DisableFinishedPage      = False
ShowTasksTreeLines       = True
AlwaysShowDirOnReadyPage = True 
UninstallDisplayIcon     = {app}\{#AppExeName}  
AllowNetworkDrive        = Yes
AllowUNCPath             = Yes
AppCopyright             = Copyright (C) 2021 SIMPLYLIVE.TV, Inc.
AppPublisher             = SIMPLYLIVE.TV, Inc.
AppPublisherURL          = http://www.simplylive.tv/
AppMutex                 = "NDIProvider.GUI.Mutex"
SetupMutex               = "NDIProvider.GUI.SetupMutex"

[Files]
Source: "psvince.dll"; DestDir: "{#ConfigPath}"; Flags: ignoreversion
Source: "Glossy.vsf"; DestDir: "{#ConfigPath}"; Flags: ignoreversion
Source: "VclStylesinno.dll"; DestDir: "{#ConfigPath}"; Flags: ignoreversion
Source: "{#Dotnetruntime}"; DestDir: "{tmp}"; Check: NeedInstallDotnetruntime
Source: "{#Aspnetcoreruntime}"; DestDir: "{tmp}"; Check: NeedInstallAspnetcoreruntime
Source: "..\Release\Backend.Network.ProtocolSdk.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.BrowserSubprocess.Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.BrowserSubprocess.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.BrowserSubprocess.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.BrowserSubprocess.runtimeconfig.json"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.Core.Runtime.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\CefSharp.OffScreen.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\chrome_100_percent.pak"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\chrome_200_percent.pak"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\chrome_elf.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\d3dcompiler_47.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\dxcompiler.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\dxil.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\icudtl.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Ijwhost.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Lib.AVFormat.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Lib.Framework.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Lib.Log.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Lib.NDI.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Lib.Serialize.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\libcef.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\libEGL.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\libGLESv2.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Microsoft.OpenApi.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Microsoft.Win32.SystemEvents.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\NDIProvider.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\NDIProvider.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\NDIProvider.runtimeconfig.json"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\NLog.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Processing.NDI.Lib.DirectShow.x64.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Processing.NDI.Lib.UWP.x64.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Processing.NDI.Lib.x64.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\protobuf-net.Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\protobuf-net.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\resources.pak"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\snapshot_blob.bin"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Swashbuckle.AspNetCore.Swagger.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Swashbuckle.AspNetCore.SwaggerGen.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\Swashbuckle.AspNetCore.SwaggerUI.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\System.Configuration.ConfigurationManager.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\System.Drawing.Common.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\System.Security.Cryptography.ProtectedData.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\v8_context_snapshot.bin"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\vk_swiftshader.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\vk_swiftshader_icd.json"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\vulkan-1.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\zlib.net.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Release\locales\af.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\am.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ar.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\bg.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\bn.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ca.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\cs.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\da.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\de.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\el.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\en-GB.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\en-US.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\es-419.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\es.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\et.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\fa.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\fi.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\fil.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\fr.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\gu.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\he.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\hi.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\hr.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\hu.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\id.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\it.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ja.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\kn.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ko.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\lt.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\lv.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ml.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\mr.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ms.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\nb.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\nl.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\pl.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\pt-BR.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\pt-PT.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ro.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ru.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\sk.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\sl.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\sr.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\sv.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\sw.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ta.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\te.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\th.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\tr.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\uk.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\ur.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\vi.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\zh-CN.pak"; DestDir: "{app}\locales"; Flags: ignoreversion
Source: "..\Release\locales\zh-TW.pak"; DestDir: "{app}\locales"; Flags: ignoreversion

[run]
Filename: "{app}\httpadd.bat"; Flags: nowait
Filename: "{tmp}\{#Dotnetruntime}"; Parameters: "/quiet /norestart"; WorkingDir: "{tmp}"; Flags: skipifdoesntexist; StatusMsg: "{#NetRuntimeName}"; Check: NeedInstallDotnetruntime
Filename: "{tmp}\{#Aspnetcoreruntime}"; Parameters: "/quiet /norestart"; WorkingDir: "{tmp}"; Flags: skipifdoesntexist; StatusMsg: "{#NetRuntimeName}"; Check: NeedInstallAspnetcoreruntime

[installDelete]
Name:"{localappdata}\Simplylive\NDIProvider"; Type: filesandordirs
Name:{app}; Type: filesandordirs

[UninstallDelete]
Name: {app}; Type: filesandordirs

[Registry]
Root: "HKLM64"; Subkey: "SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps\{#AppExeName}"; ValueType: string; ValueName: "DumpFolder"; ValueData: "C:\Logs\Dump"; Flags: uninsdeletekey; Check: IsWin64;
Root: "HKLM64"; Subkey: "SOFTWARE\Microsoft\Windows\Windows Error Reporting\LocalDumps\{#AppExeName}"; ValueType: dword; ValueName: "DumpType"; ValueData: "1"; Flags: uninsdeletekey; Check: IsWin64;

[Messages]
english.BeveledLabel=SIMPLYLIVE.TV

[Languages]
Name: "english"; MessagesFile: "compiler:default.isl"

[Types]
Name: "custom"; Description: "Custom installation"; Flags: iscustom;

[Components] 
Name: {#Componentnetnet};      Description: {#netruntimename};    Types: custom;   Flags: fixed disablenouninstallwarning;

[Icons]
Name: "{group}\{#AppExeName}"; Filename: "{app}\{#AppExeName}"

[Dirs]
Name: "{app}\locales"

[Code]
type
    INSTALLSTATE = Longint;

const
    INSTALLSTATE_INVALIDARG = -2;  // An invalid parameter was passed to the function.
    INSTALLSTATE_UNKNOWN = -1;     // The product is neither advertised or installed.
    INSTALLSTATE_ADVERTISED = 1;   // The product is advertised but not installed.
    INSTALLSTATE_ABSENT = 2;       // The product is installed for a different user.
    INSTALLSTATE_DEFAULT = 5;      // The product is installed for the current user.
type
    HANDLE = cardinal;


procedure KillTask(FileName: String);
var
  ResultCode: Integer;
begin
    Exec(ExpandConstant('taskkill.exe'), '/f /im ' + '"' + FileName + '"', '', SW_HIDE, ewWaitUntilTerminated, ResultCode);
end;
var 
  dotnetruntimeMissing: Boolean;
 
function NeedInstallDotnetruntime(): Boolean;
begin
  Result := dotnetruntimeMissing;
end;

procedure DotnetruntimeInstall();
var
    ResultCode: Integer;
begin	
    if RegKeyExists(HKEY_CLASSES_ROOT,'Installer\Dependencies\{72922c3b-f4df-4f93-9e3b-5b9c8a5ffb42}') then
      begin
         dotnetruntimeMissing:=false;
      end 
    else
     begin                
       dotnetruntimeMissing:=true;
     end;
end;


var 
  aspnetcoreruntimeMissing: Boolean;

function NeedInstallAspnetcoreruntime(): Boolean;
begin
  Result := aspnetcoreruntimeMissing;
end;

procedure AspnetcoreruntimeInstall();
var
    ResultCode: Integer;
begin	
    if RegKeyExists(HKEY_CLASSES_ROOT,'Installer\Dependencies\{b1bba97c-11ce-43c2-9ca3-af05b7269fcc}') then
      begin
         aspnetcoreruntimeMissing:=false;
      end 
    else
     begin                
       aspnetcoreruntimeMissing:=true;
     end;
end; 

function IsModuleLoaded(modulename: AnsiString): Boolean; external 'IsModuleLoaded@files:psvince.dll stdcall';
function IsModuleLoadedx64(modulename: AnsiString): Boolean; external 'IsModuleLoaded2@files:psvince.dll stdcall';
function IsModuleLoadedU(modulename: AnsiString): Boolean; external 'IsModuleLoaded@{#ConfigPath}\psvince.dll stdcall uninstallonly';
function IsModuleLoadedUx64(modulename: AnsiString): Boolean; external 'IsModuleLoaded2@{#ConfigPath}\psvince.dll stdcall uninstallonly';
procedure LoadVCLStyle(VClStyleFile: String); external 'LoadVCLStyleW@files:VclStylesInno.dll stdcall';
procedure LoadVCLStyle_UnInstall(VClStyleFile: String); external 'LoadVCLStyleW@{#ConfigPath}\VclStylesInno.dll stdcall uninstallonly';
procedure UnLoadVCLStyles; external 'UnLoadVCLStyles@files:VclStylesInno.dll stdcall setuponly';
procedure UnLoadVCLStyles_UnInstall; external 'UnLoadVCLStyles@{#ConfigPath}\VclStylesInno.dll stdcall uninstallonly';

function InitializeSetup(): Boolean;
var 
  running: Boolean;

begin
   ExtractTemporaryFile('{#VCLStyle}');
	 LoadVCLStyle(ExpandConstant('{tmp}\{#VCLStyle}'));
   DotnetruntimeInstall();
   AspnetcoreruntimeInstall();
   running := IsModuleLoadedx64('{#AppWindowName}');
   if (running) then
   begin
        KillTask('{#AppExeName}');
   end;
   Result := true; 
end;

procedure DeinitializeSetup();
begin
	UnLoadVCLStyles;
end;

function InitializeUninstall(): Boolean;
var running: Boolean;    
begin
   LoadVCLStyle_UnInstall(ExpandConstant('{#ConfigPath}\{#VCLStyle}'));
   running := IsModuleLoadedUx64('{#AppExeName}');
   if (running) then
   begin
        KillTask('{#AppExeName}');
   end;
   Result := true;
end;

procedure DeinitializeUninstall();
begin
  UnLoadVCLStyles_UnInstall;
end;

procedure InitializeWizard();
begin
  WizardForm.PAGEDESCRIPTIONLABEL.width:=0;
  WizardForm.WizardSmallBitmapImage.Stretch := false;
  WizardForm.WizardSmallBitmapImage.width:=149;
  WizardForm.WizardSmallBitmapImage.left:=WizardForm.width-159;
  WizardForm.PageNameLabel.Width := WizardForm.width-250; 
end;
