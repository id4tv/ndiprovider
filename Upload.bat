@echo off
setlocal enabledelayedexpansion

set "fileName="
for %%F in (%cd%\setup\Output\*.exe) do (
    set "fileName=!fileName!%%~nxF"
)

set "refreshToken=f_D5RF2FuXAAAAAAAAAAAX8Ks_lI6R9HcqYz39iRTELC9PufuSO6QZ9DqWS2Ipau"
set "appKey=ibqi0xcaxsgoffn"
set "appSecret=yeasn6e058vfcqg"
set "localFile=%cd%\setup\Output\%fileName%"
set "destinationPath=/R^&D/Internal^ Release/NDIProvider/%fileName%"
set "SlackWebhookURL=https://hooks.slack.com/services/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"

echo FILENAME: %fileName%
echo localFile: %localFile%

:: Get dropbox access token
for /f "delims=" %%r in ('curl -X POST https://api.dropbox.com/oauth2/token ^
   -d refresh_token^=%refreshToken% ^
   -d grant_type^=refresh_token ^
   -d client_id^=%appKey% ^
   -d client_secret^=%appSecret%') do (set jsonToken=%%r)

for /f "delims=" %%a in ('echo !jsonToken! ^| jq ".access_token"') do set accessToken=%%a
echo AccessToken %accessToken%

:: Get upload session ID
curl -X POST https://content.dropboxapi.com/2/files/upload_session/start ^
     -H "Authorization: Bearer %accessToken%" ^
     -H "Dropbox-API-Path-Root: {\".tag\": \"root\", \"root\": \"2559689395\"}"^
     -H "Content-Type: application/octet-stream" ^
     --data-binary @NUL > response.json

for /f "delims=" %%i in (response.json) do set jsonSession=%%i
for /f "delims=" %%a in ('echo !jsonSession! ^| jq ".session_id"') do set session_id=%%a
:: Remove temporary response.json file
del response.json
echo Session ID: %session_id%

:: Get file size
for %%F in ("%localFile%") do set "fileSize=%%~zF"
echo FileSize %fileSize%

:: Split file in chunks of 140mb
echo Splitting File
set size=140
powershell Split-File '%localFile%' %size%mb
echo File successfully splitted

:: Set chunkSize 140M
set maxChunkSize=146800640

set /a numChunks=%fileSize% / %maxChunkSize%
set /a remainder=%fileSize% %% %maxChunkSize%
if %remainder% neq 0 set /a numChunks+=1
echo ChunksNumber %numChunks%

for /l %%i in (1, 1, %numChunks%) do (
    set /a n=%%i - 1
    echo N !n!
	set "chunk=%localFile%.!n!.part"
	echo CHUNK !chunk!

	set /a "startByte = (%%i - 1) * maxChunkSize"
	::if %%i NEQ 1 set /a startByte += 1
	echo STARTBYTE !startByte!

	set /a "endByte=%%i * %maxChunkSize%"
	if %%i equ %numChunks% set endByte=%fileSize%
	echo ENDBYTE !endByte!

	:: Upload current part
    curl -X POST https://content.dropboxapi.com/2/files/upload_session/append_v2^
         -H "Authorization: Bearer %accessToken%"^
         -H "Dropbox-API-Path-Root: {\".tag\": \"root\", \"root\": \"2559689395\"}"^
         -H "Content-Type: application/octet-stream"^
         -H "Dropbox-API-Arg: {\"cursor\":{\"session_id\":\"!session_id!\",\"offset\":!startByte!},\"close\":false}"^
         -T !chunk! -o response.json

    :: Get cursor for next part
    for /f "delims=" %%j in (response.json) do set cursor=%%j

    :: Remove temporary response.json file
    del response.json

    echo Part !n! upload successfull. Cursor : !cursor!

    :: Remove temporary chunk file
    del !chunk!

    echo delete !chunk! successfull
)

:: Terminate upload session
curl -X POST https://content.dropboxapi.com/2/files/upload_session/finish^
     -H "Authorization: Bearer %accessToken%"^
     -H "Dropbox-API-Path-Root: {\".tag\": \"root\", \"root\": \"2559689395\"}"^
     -H "Content-Type: application/octet-stream"^
     -H "Dropbox-API-Arg: {\"cursor\":{\"session_id\":\"!session_id!\",\"offset\":%fileSize%},\"commit\":{\"path\":\"%destinationPath%\",\"mode\":\"add\",\"autorename\":true,\"mute\":false}}"^
     --data-binary @NUL -o response.json
del response.json
echo Upload successfull.

:: Get link
curl -X POST "https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings" ^
	-H "Authorization: Bearer %accessToken%" ^
    -H "Dropbox-API-Path-Root: {\".tag\": \"root\", \"root\": \"2559689395\"}"^
    -H "Content-Type: application/json" ^
    --data "{\"path\":\"%destinationPath%\"}" > response.json

for /f "delims=" %%i in (response.json) do set jsonLink=%%i
for /f "delims=" %%a in ('echo !jsonLink! ^| jq ".url"') do set share_link=%%a
del response.json
echo Link: !share_link!

:: Send Notif
curl -X POST !SlackWebhookURL! ^
    -H "Content-type: application/json" ^
    --data "{\"text\":\"!fileName!:\n!share_link!\"}"
echo Notification sent to Slack.

endlocal