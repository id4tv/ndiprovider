using System.Reflection;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using RefService.Rest;

namespace NDIProvider.Rest
{
    public class RestService
    {
        //private const string myAllowSpecificOrigins = "cors";
        private const int _httpport = 20050;

        public static void Init()
        {
            var builder = WebApplication.CreateBuilder();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddHttpClient();
            builder.Services.AddHttpContextAccessor();
            builder.Services.AddEndpointsApiExplorer();
            //builder.Services.AddHealthChecks();
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "NDIProvider API (debug)",
                        Version = Assembly.GetExecutingAssembly().GetName().Version?.ToString()
                    });
            });
            builder.Services.AddMvc().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                options.JsonSerializerOptions.Encoder = JavaScriptEncoder.UnsafeRelaxedJsonEscaping;
            });
            //builder.Services.AddCors(options =>
            //{
            //    options.AddPolicy(myAllowSpecificOrigins, bd => bd.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            //});

            WebApplication app = builder.Build();

            // Configure the HTTP request pipeline.
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "NDIProvider API(v1)");
                c.RoutePrefix = string.Empty;
            });

            //app.UseCors(myAllowSpecificOrigins);

            Routes.Init(app);

            app.Run($"http://*:{_httpport}");
        }
    }
}