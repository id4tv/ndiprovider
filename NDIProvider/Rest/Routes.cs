﻿using System.Reflection;
using System.Threading.Tasks;
using Lib.Framework;
using Lib.Serialize;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NDIProvider;

namespace RefService.Rest
{
    public sealed class Routes
    {
        public static void Init(WebApplication app)
        {
            //app.MapHealthChecks("/health");
            app.MapGet("/v1/heartbeat", Heartbeat);
            app.MapPost("/v1/startStream", CreateBitcEngine);
            app.MapPost("/v1/stream", CreateBitcEngine);
            app.MapPost("/v1/streamWithNoName", CreateEngine);
            app.MapPost("/v1/streamTest", CreateTestEngine);
            app.MapDelete("/v1/stopStream", DeleteEngine);
            app.MapDelete("/v1/stream", DeleteEngine);
        }

        private static ValueTask<string> Heartbeat()
        {
            Version version = new Version
            {
                version = Assembly.GetAssembly(typeof(Routes))?.GetName().Version.ToVersionNum()
            };
            var result = version.ToJson();
            return ValueTask.FromResult(result);
        }

        private static async ValueTask<string> CreateBitcEngine([FromBody] Paramter parameter, HttpResponse response)
        {
            var result = await EngineManage.CreateBitcEngine(parameter);
            if (result.Code != 1) response.StatusCode = result.StatusCode;
            return await ValueTask.FromResult(result.ToJson());
        }

        private static async ValueTask<string> CreateEngine([FromBody] Paramter parameter, HttpResponse response)
        {
            var result = await EngineManage.CreateEngine(parameter);
            if (result.Code != 1) response.StatusCode = result.StatusCode;
            return await ValueTask.FromResult(result.ToJson());
        }

        private static async ValueTask<string> CreateTestEngine([FromBody] TestParamter parameter, HttpResponse response)
        {
            var result = await EngineManage.CreateTestEngine(parameter);
            return await ValueTask.FromResult(result.ToJson());
        }

        private static async ValueTask<string> DeleteEngine([FromBody] Paramter parameter, HttpResponse response)
        {
            var result = EngineManage.DeleteEngine(parameter);
            return await ValueTask.FromResult(result.ToJson());
        }
    }

    class Version
    {
        public string version { get; set; }
    }
}
