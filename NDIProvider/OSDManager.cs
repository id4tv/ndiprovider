﻿using System;
using System.Threading.Tasks;
using Lib.Log;
using Lib.Serialize;

namespace NDIProvider
{
    public class OSDManager : IDisposable
    {
        private static RESTClient _restClient;
        private const string _osdProviderIp = "127.0.0.1";
        private const int _osdProviderPort = 20022;

        public OSDManager()
        {
            _restClient = new RESTClient(_osdProviderIp, _osdProviderPort);
        }

        public async Task<OSDResult> GetOsdResult(Paramter para)
        {
            OSD osd = new OSD { backendIp = para.backendIp, channelId = para.channel };
            var restApiResult = await _restClient.CallRestAPIByGet($"http://{_osdProviderIp}:{_osdProviderPort}/v1/framedplayer", osd.ToJson());
            if (restApiResult.Success)
            {
                OSDResult result = restApiResult.Message.ToObject<OSDResult>();
                Logger.Info("NDIEngine.log", $"OSD info: {restApiResult.Message}");
                return result;
            }
            Logger.Error("NDIEngine.log", $"get osd info error! {restApiResult.Message}");
            return null;
        }

        public async Task<OSDResult> GetBitcPlayerOsd(Paramter para)
        {
            OSD osd = new OSD { backendIp = para.backendIp, channelId = para.channel };
            var restApiResult = await _restClient.CallRestAPIByGet($"http://{_osdProviderIp}:{_osdProviderPort}/v1/BitcPlayerOsd", osd.ToJson());
            if (restApiResult.Success)
            {
                OSDResult result = restApiResult.Message.ToObject<OSDResult>();
                Logger.Info("NDIEngine.log", $"Bitc OSD info: {restApiResult.Message}");
                return result;
            }
            Logger.Error("NDIEngine.log", $"get bitc osd info error! {restApiResult.Message}");
            return null;
        }


        public void Dispose()
        {
            _restClient.Dispose();
        }
    }
}
