﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NDIProvider
{
    public class RESTClient : IDisposable
    {
        private HttpClient _httpClient;
        private HttpClientHandler _handler;

        public RESTClient(string ip, int port)
        {
            _handler = new HttpClientHandler();
            _handler.AllowAutoRedirect = false;
            _handler.Proxy = null;
            _handler.UseProxy = false;
            _httpClient = new HttpClient(_handler)
            {
                Timeout = new TimeSpan(0, 0, 20), BaseAddress = new Uri($"http://{ip}:{port}/")
            };
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
        }

        public async Task<RestAPIResult> CallRestAPIByPut(string url, string data)
        {
            try
            {
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = await _httpClient.PutAsync(url, content);
                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();
                return new RestAPIResult { Success = true, Message = result };
            }
            catch (Exception e)
            {
                return new RestAPIResult { Success = false, Message = "CallRestAPIByPut error. " + e.Message };
            }
        }

        public async Task<RestAPIResult> CallRestAPIByGet(string url)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.GetAsync(url);
                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();
                return new RestAPIResult { Success = true, Message = result };
            }
            catch (Exception e)
            {
                return new RestAPIResult { Success = false, Message = "CallRestAPIByGet error. " + e.Message };
            }
        }

        public async Task<RestAPIResult> CallRestAPIByGet(string url, string data)
        {
            try
            {
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpRequestMessage httpRequest = new HttpRequestMessage(HttpMethod.Get, url) {Content = content};
                HttpResponseMessage response = await _httpClient.SendAsync(httpRequest);
                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();
                return new RestAPIResult { Success = true, Message = result };
            }
            catch (Exception e)
            {
                return new RestAPIResult { Success = false, Message = "CallRestAPIByGet error. " + e.Message };
            }
        }

        public async Task<RestAPIResult> CallRestAPIByPost(string url, string data)
        {
            try
            {
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage response = await _httpClient.PostAsync(url, content);
                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();
                return new RestAPIResult { Success = true, Message = result };
            }
            catch (Exception e)
            {
                return new RestAPIResult { Success = false, Message = "CallRestAPIByPost error. " + e.Message };
            }
        }

        public async Task<RestAPIResult> CallRestAPIByDelete(string url)
        {
            try
            {
                HttpResponseMessage response = await _httpClient.DeleteAsync(url);
                response.EnsureSuccessStatusCode();

                var result = await response.Content.ReadAsStringAsync();
                return new RestAPIResult { Success = true, Message = result };
            }
            catch (Exception e)
            {
                return new RestAPIResult { Success = false, Message = "CallRestAPIByDelete error. " + e.Message };
            }
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
            _httpClient = null;
            _handler?.Dispose();
            _handler = null;
        }
    }

    public class RestAPIResult
    {
        public bool Success;
        public string Message;
    }

    public class GeneralResult
    {
        public bool Failed { get; set; }

        public string ErrorMessage { get; set; }
    }
}
