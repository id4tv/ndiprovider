﻿using System;
using Lib.Framework.Mutex;
using Lib.Log;
using NDIProvider.Rest;

namespace NDIProvider
{
    class Program
    {
        public static int Main(string[] args)
        {
            // 28ac37db-12aa-4f8f-9340-397095e62ac1
            Logger.Init($@"{Logger.GetUserPath()}\NDIProvider");
            bool createdNew = MutexHelper.CreateMutex("NDIProvider");
            if (createdNew)
            {
                AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
                EngineManage.Init();
                RestService.Init();
                EngineManage.Dispose();
            }
            else
            {
                Logger.Info("NDIProvider.log", "Another NDIProvider application has been started!");
                Logger.Dispose();
                Environment.Exit(0);
            }
            return 0;
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Error("NDIProvider.log", "OnUnhandledException", (Exception)e.ExceptionObject);
        }
    }
}
