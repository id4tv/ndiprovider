﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Timers;
using CefSharp;
using CefSharp.OffScreen;
using Lib.AVFormat;
using Lib.Log;
using Lib.NDI;
using Timer = System.Timers.Timer;

namespace NDIProvider
{
    public class Engine : IDisposable
    {
        private SendManager _sendManager;
        private ChromiumWebBrowser _browser;
        private DateTime _lastTime;
        private DateTime _firstTime;
        private Timer _timer;
        private IntPtr _lastFrame;
        private const int _timeOut = 1000;
        private bool _disposed;
        private long _frameCount = 0;

        public Engine(string ndiName)
        {
            NDIName = ndiName;
            _sendManager = new SendManager();
        }

        public int Channel { get; set; }

        public string NDIName { get; set; }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_lastFrame != IntPtr.Zero)
            {
                DateTime time = DateTime.Now;
                TimeSpan diff = time - _lastTime;
                if (diff.TotalMilliseconds > _timeOut)
                {
                    _sendManager?.Send(_lastFrame);
                    //Console.WriteLine($"Send buffer by _timer_Elapsed:-------{DateTime.Now:yyyy-MM-dd hh:mm:ss fff}---------");
                }
            }
            else
            {
                Logger.Info("NDIEngine.log", $"have no frame from CEF. {NDIName}");
            }
        }

        public async Task Init(string url, FormatInfo formatInfo)
        {
            try
            {
                Logger.Info("NDIEngine.log", $"Init {NDIName}, framerate: {formatInfo.FrameRate}, Width: {formatInfo.Width}, Height: {formatInfo.Height}");

                var browserSettings = new BrowserSettings(true) { WindowlessFrameRate = formatInfo.FrameRate };
                //Reduce rendering speed to one frame per second so it's easier to take screen shots
                var requestContextSettings = new RequestContextSettings
                {
                    CachePath = Path.GetFullPath("cache")
                };
                //RequestContext can be shared between browser instances and allows for custom settings

                //e.g.CachePath

                var requestContext = new RequestContext(requestContextSettings);
                _browser = new ChromiumWebBrowser(url, browserSettings, requestContext)
                {
                    Size = new Size(formatInfo.Width, formatInfo.Height)
                };

                await WaitInitAsync(_browser);

                _browser.Paint += BrowserOnPaint;
                _browser.LoadError += _browser_LoadError;

                _timer = new Timer(_timeOut);
                _timer.Elapsed += _timer_Elapsed;
                _timer.Start();
                _sendManager.Init(NDIName, formatInfo);
            }
            catch (Exception e)
            {
                Logger.Error("NDIEngine.log", "Init error", e);
            }
        }

        private async Task WaitInitAsync(ChromiumWebBrowser browser)
        {
            try
            {
                var tcsBrowserInitialized = new TaskCompletionSource<bool>();
                var tcsLoadingStateChanged = new TaskCompletionSource<bool>();

                EventHandler handler = null;
                handler = (_, _) =>
                {
                    browser.BrowserInitialized -= handler;
                    Logger.Info("NDIEngine.log", $"BrowserInitialized {NDIName}:-------{DateTime.Now:yyyy-MM-dd hh:mm:ss fff}---------");
                    tcsBrowserInitialized.SetResult(true);
                };

                browser.BrowserInitialized += handler;

                EventHandler<LoadingStateChangedEventArgs> handler1 = null;
                handler1 = (_, e) =>
                {
                    if (!e.IsLoading)
                    {
                        browser.LoadingStateChanged -= handler1;
                        Logger.Info("NDIEngine.log", $"loadingStateChanged {NDIName}:-------{DateTime.Now:yyyy-MM-dd hh:mm:ss fff}---------");
                        tcsLoadingStateChanged.SetResult(true);
                    }
                };

                browser.LoadingStateChanged += handler1;

                await Task.WhenAll(tcsBrowserInitialized.Task, tcsLoadingStateChanged.Task);
            }
            catch (Exception e)
            {
                Logger.Error("NDIEngine.log", "WaitInitAsync error", e);
            }
        }

        private void _browser_LoadError(object sender, LoadErrorEventArgs e)
        {
            Logger.Info("NDIEngine.log", $"LoadError {NDIName}-------{DateTime.Now:yyyy-MM-dd hh:mm:ss fff}---------");
        }

        private void BrowserOnPaint(object sender, OnPaintEventArgs e)
        {
            if (_disposed) return;
            //byte[] buffer = new byte[bufferSize];
            //Marshal.Copy(source, buffer, 0, buffer.Length);
            //var screenshotPath = Path.Combine(@"E:\cef", "CefSharp screenshot" + DateTime.Now.Ticks + ".data");
            //File.WriteAllBytes(screenshotPath, buffer);
            //var bmp = new Bitmap(e.Width, e.Height, e.Width * 4, System.Drawing.Imaging.PixelFormat.Format32bppArgb, e.BufferHandle);
            //bmp.Save($"e:\\cef\\cef_test_{DateTime.Now.Ticks}.bmp");
            _lastFrame = e.BufferHandle;
            _sendManager?.Send(e.BufferHandle);
            _lastTime = DateTime.Now;
            if (_frameCount == 0) _firstTime = _lastTime;
            _frameCount++;
            if (_frameCount == 100)
            {
                DateTime time = DateTime.Now;
                TimeSpan diff = time - _firstTime;
                Logger.Info($"NDIEngine_{NDIName}.log", $"frame count: {_frameCount}, cost time: {diff.TotalSeconds} s");
                _frameCount = 0;
            }
            //Console.WriteLine($"Send buffer:-------{DateTime.Now:yyyy-MM-dd hh:mm:ss fff}---------Width: {e.Width},Height: {e.Height}, memory: {e.BufferHandle}");
        }

        public void Dispose()
        {
            try
            {
                _disposed = true;
                if (_browser != null)
                {
                    _browser.Paint -= BrowserOnPaint;
                    _browser.LoadError -= _browser_LoadError;
                    _browser.Dispose();
                    _browser = null;
                }

                if (_timer != null)
                {
                    _timer.Dispose();
                    _timer.Elapsed -= _timer_Elapsed;
                    _timer = null;
                }
                _lastFrame = IntPtr.Zero;
                
                _sendManager?.Dispose();
                _sendManager = null;
            }
            catch (Exception e)
            {
                Logger.Error("NDIEngine.log", $"dispose error {NDIName}", e);
            }
        }
    }
}
