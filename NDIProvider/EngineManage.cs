﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.OffScreen;
using Lib.AVFormat;
using Lib.Log;
using Microsoft.AspNetCore.Http;
using NewTek;

namespace NDIProvider
{
    public class EngineManage
    {
        private static readonly ConcurrentDictionary<string, Engine> _handlers = new ConcurrentDictionary<string, Engine>();
        private static readonly OSDManager _osdManager = new OSDManager();

        public static void Init()
        {
            Cef.EnableWaitForBrowsersToClose();
            CefSettings settings = new CefSettings
            {
                WindowlessRenderingEnabled = true,
                IgnoreCertificateErrors = true,
                ExternalMessagePump = false,
                LogSeverity = LogSeverity.Disable,
            };
            settings.SetOffScreenRenderingBestPerformanceArgs();
            Cef.Initialize(settings);

            if (!NDIlib.initialize())
            {
                Logger.Error("NDIEngine.log", !NDIlib.is_supported_CPU() ? "CPU incompatible with NDI." : "Unable to initialize NDI.");
            }

            Logger.Info("NDIEngine.log", "Init engineManage");
        }

        public static void Dispose()
        {
            foreach (KeyValuePair<string, Engine> engine in _handlers)
            {
                engine.Value.Dispose();
            }
            _handlers.Clear();

            _osdManager.Dispose();

            NDIlib.destroy();

            //Wait until the browser has finished closing (which by default happens on a different thread).
            //Cef.EnableWaitForBrowsersToClose(); must be called before Cef.Initialize to enable this feature
            //See https://github.com/cefsharp/CefSharp/issues/3047 for details
            Cef.WaitForBrowsersToClose();

            // Clean up Chromium objects. You need to call this in your application otherwise
            // you will get a crash when closing.
            Cef.Shutdown();
        }

        public static async Task<Result> CreateBitcEngine(Paramter para)
        {
            Result rs = new Result();
            try
            {
                Logger.Info("NDIEngine.log", $"CreateBitcEngine: {para.ndiName} {para.channel} {para.backendIp} {para.format}");
                if (!string.IsNullOrEmpty(para.ndiName))
                {
                    if (!_handlers.TryGetValue(para.ndiName, out var engine1))
                    {
                        OSDResult result = await _osdManager.GetBitcPlayerOsd(para);
                        if (result != null)
                        {
                            if (!string.IsNullOrEmpty(result.url))
                            {
                                Engine engine = new Engine(para.ndiName)
                                {
                                    Channel = para.channel,
                                    NDIName = para.ndiName
                                };
                                await engine.Init(result.url, VideoFormatHelper.AnalyzeFormat((VideoFormat)para.format));

                                _handlers.TryAdd(para.ndiName, engine);
                                rs.Message = $"create stream channel success! {result.url}";
                                rs.Code = 1;
                                rs.StatusCode = StatusCodes.Status200OK;
                            }
                            else
                            {
                                rs.Code = 2;
                                rs.StatusCode = StatusCodes.Status500InternalServerError;
                                rs.Message = $"result bitc osd url is empty! {result.error}";
                            }
                        }
                        else
                        {
                            rs.Code = 2;
                            rs.StatusCode = StatusCodes.Status500InternalServerError;
                            rs.Message = "get bitc osd info eror! can't get the bitc osd info";
                        }
                    }
                    else
                    {
                        if (engine1.Channel == para.channel)
                        {
                            rs.Message = $"create stream channel fail, already have this channel. {para.channel}";
                            rs.Code = 3;
                            rs.StatusCode = StatusCodes.Status200OK;
                        }
                        else
                        {
                            OSDResult result = await _osdManager.GetBitcPlayerOsd(para);
                            if (result != null)
                            {
                                if (!string.IsNullOrEmpty(result.url))
                                {
                                    engine1.Channel = para.channel;
                                    rs.Message = $"update stream channel source success! {result.url}";
                                    rs.Code = 1;
                                    rs.StatusCode = StatusCodes.Status200OK;
                                }
                                else
                                {
                                    rs.Message = $"result bitc osd url is empty! {result.error}";
                                    rs.Code = 2;
                                    rs.StatusCode = StatusCodes.Status500InternalServerError;
                                }
                            }
                        }
                    }
                }
                else
                {
                    rs.Code = 2;
                    rs.StatusCode = StatusCodes.Status400BadRequest;
                    rs.Message = "ndi name is empty!";
                }
                Logger.Info("NDIEngine.log", $"CreateBitcEngine {para.ndiName}, {rs.Message}");
            }
            catch (Exception e)
            {
                rs.Code = 2;
                rs.StatusCode = StatusCodes.Status500InternalServerError;
                rs.Message = $"face some error: {e.Message}";
                Logger.Error("NDIEngine.log", $"CreateBitcEngine error {para.ndiName}", e);
            }
            return rs;
        }

        public static async Task<Result> CreateEngine(Paramter para)
        {
            Result rs = new Result();
            try
            {
                Logger.Info("NDIEngine.log", $"CreateEngine: {para.ndiName} {para.channel} {para.backendIp} {para.format}");
                if (!string.IsNullOrEmpty(para.ndiName))
                {
                    if (!_handlers.TryGetValue(para.ndiName, out var engine1))
                    {
                        OSDResult result = await _osdManager.GetOsdResult(para);
                        if (result != null)
                        {
                            if (!string.IsNullOrEmpty(result.url))
                            {
                                Engine engine = new Engine(para.ndiName)
                                {
                                    Channel = para.channel,
                                    NDIName = para.ndiName
                                };
                                await engine.Init(result.url, VideoFormatHelper.AnalyzeFormat((VideoFormat)para.format));

                                _handlers.TryAdd(para.ndiName, engine);
                                rs.Message = $"create stream channel success! {result.url}";
                                rs.Code = 1;
                                rs.StatusCode = StatusCodes.Status200OK;
                            }
                            else
                            {
                                rs.Code = 2;
                                rs.StatusCode = StatusCodes.Status500InternalServerError;
                                rs.Message = $"result osd url is empty! {result.error}";
                            }
                        }
                        else
                        {
                            rs.Code = 2;
                            rs.StatusCode = StatusCodes.Status500InternalServerError;
                            rs.Message = "get osd info eror! can't get the osd info";
                        }
                    }
                    else
                    {
                        if (engine1.Channel == para.channel)
                        {
                            rs.Message = $"create stream channel fail, already have this channel. {para.channel}";
                            rs.Code = 3;
                            rs.StatusCode = StatusCodes.Status200OK;
                        }
                        else
                        {
                            OSDResult result = await _osdManager.GetOsdResult(para);
                            if (result != null)
                            {
                                if (!string.IsNullOrEmpty(result.url))
                                {
                                    engine1.Channel = para.channel;
                                    rs.Message = $"update stream channel source success! {result.url}";
                                    rs.Code = 1;
                                    rs.StatusCode = StatusCodes.Status200OK;
                                }
                                else
                                {
                                    rs.Message = $"result osd url is empty! {result.error}";
                                    rs.Code = 2;
                                    rs.StatusCode = StatusCodes.Status500InternalServerError;
                                }
                            }
                        }
                    }
                }
                else
                {
                    rs.Code = 2;
                    rs.StatusCode = StatusCodes.Status400BadRequest;
                    rs.Message = "ndi name is empty!";
                }
                Logger.Info("NDIEngine.log", $"CreateEngine {para.ndiName}, {rs.Message}");
            }
            catch (Exception e)
            {
                rs.Code = 2;
                rs.StatusCode = StatusCodes.Status500InternalServerError;
                rs.Message = $"face some error: {e.Message}";
                Logger.Error("NDIEngine.log", $"CreateEngine error {para.ndiName}", e);
            }
            return rs;
        }

        public static async Task<Result> CreateTestEngine(TestParamter para)
        {
            Result rs = new Result();
            try
            {
                Logger.Info("NDIEngine.log", $"CreateTestEngine: {para.ndiName} {para.channel} {para.backendIp} {para.format} {para.url}");
                if (!string.IsNullOrEmpty(para.ndiName))
                {
                    if (!_handlers.TryGetValue(para.ndiName, out var engine1))
                    {
                        Engine engine = new Engine(para.ndiName)
                        {
                            Channel = para.channel,
                            NDIName = para.ndiName
                        };
                        await engine.Init(para.url, VideoFormatHelper.AnalyzeFormat((VideoFormat)para.format));

                        _handlers.TryAdd(para.ndiName, engine);
                        rs.Message = $"create stream channel success! {para.url}";
                        rs.Code = 1;
                        rs.StatusCode = StatusCodes.Status200OK;
                    }
                    else
                    {
                        if (engine1.Channel == para.channel)
                        {
                            rs.Message = "create stream channel fail, already have this channel.";
                            rs.Code = 3;
                            rs.StatusCode = StatusCodes.Status200OK;
                        }
                        else
                        {
                            engine1.Channel = para.channel;
                            rs.Message = $"update stream channel source success! {para.url}";
                            rs.Code = 1;
                            rs.StatusCode = StatusCodes.Status200OK;
                        }
                    }
                }
                else
                {
                    rs.Code = 2;
                    rs.StatusCode = StatusCodes.Status400BadRequest;
                    rs.Message = "ndi name is empty!";
                }
                Logger.Info("NDIEngine.log", $"CreateTestEngine {para.ndiName}, {rs.Message}");
            }
            catch (Exception e)
            {
                rs.Code = 2;
                rs.StatusCode = StatusCodes.Status500InternalServerError;
                rs.Message = $"face some error: {e.Message}";
                Logger.Error("NDIEngine.log", $"CreateTestEngine error {para.ndiName}", e);
            }
            return rs;
        }

        public static Task<Result> DeleteEngine(Paramter para)
        {
            Result rs = new Result();
            try
            {
                Logger.Info("NDIEngine.log", $"DeleteEngine: {para.ndiName} {para.channel} {para.backendIp} {para.format}");
                if (!string.IsNullOrEmpty(para.ndiName))
                {
                    if (_handlers.TryRemove(para.ndiName, out var engine))
                    { 
                        engine.Dispose();
                        Logger.Info("NDIEngine.log", $"remvoe from the _handlers for the name {para.ndiName}");
                    }
                    rs.Message = $"dispose stream channel success! {para.ndiName}";
                    rs.Code = 1;
                    rs.StatusCode = StatusCodes.Status200OK;
                }
                else
                {
                    rs.Code = 2;
                    rs.StatusCode = StatusCodes.Status400BadRequest;
                    rs.Message = "ndi name is empty!";
                }
                Logger.Info("NDIEngine.log", $"DeleteEngine {para.ndiName}, {rs.Message}");
            }
            catch (Exception e)
            {
                rs.Code = 2;
                rs.StatusCode = StatusCodes.Status500InternalServerError;
                rs.Message = $"face some error: {e.Message}";
                Logger.Error("NDIEngine.log", $"DeleteEngine error {para.ndiName}", e);
            }

            return Task.FromResult(rs);
        }
    }

    public class Paramter
    {
        public string ndiName { get; set; }
        public int channel { get; set; }
        public int format { get; set; }
        public string backendIp { get; set; }
    }

    public class TestParamter : Paramter
    {
        public string url { get; set; }
    }

    public class OSD
    {
        public string backendIp { get; set; }
        public int channelId { get; set; }
    }

    public class OSDResult
    {
        public string error { get; set; }
        public int code { get; set; }
        public string url { get; set; }
    }

    public sealed class Result
    {
        /// <summary>
        /// HTTP response code.
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Internal code. 1, sucess
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Code message.
        /// </summary>
        public string Message { get; set; }
    }
}
