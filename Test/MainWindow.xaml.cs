﻿using System;
using System.Drawing;
using System.IO;
using System.Windows;
using CefSharp;
using Lib.AVFormat;
using Lib.NDI;

namespace Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly SendManager _sender = new SendManager();
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs ex)
        {
            var formatInfo = VideoFormatHelper.AnalyzeFormat((VideoFormat) 2);
            browser.Width = formatInfo.Width;
            browser.Height = formatInfo.Height;
            _sender.Init("Live Gfx dxh2", formatInfo);
            jsonURL.Text = Path.Combine(Environment.CurrentDirectory, "Json.txt");
            tbURL.Text = Path.Combine(Environment.CurrentDirectory, "Test.html");
            browser.Address = tbURL.Text;
        }


        private void ChromiumWebBrowser_Paint(object sender, CefSharp.Wpf.PaintEventArgs e)
        {
            //{
            //var bmp = new Bitmap(e.Width, e.Height, e.Width * 4, System.Drawing.Imaging.PixelFormat.Format32bppArgb, e.Buffer);
            //bmp.Save($"e:\\cef\\cef_test_{DateTime.Now.Ticks}.bmp");
            //}
            //string sss = $"witdh: {e.Width}, height: {e.Height}";
            //if (e.Width != 1920 || e.Height != 1080) return;

            //byte[] buffer = new byte[e.Width * e.Height * 4];
            //Marshal.Copy(e.Buffer, buffer, 0, buffer.Length);

            _sender.Send(e.Buffer);

            //IntPtr buffer = Marshal.AllocHGlobal(1920 * 1080 * 4);
            //unsafe
            //{
            //    Buffer.MemoryCopy(e.Buffer.ToPointer(), buffer.ToPointer(), 1920 * 1080 * 4, 1920 * 1080 * 4);
            //}

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            browser.Address = tbURL.Text;
        }

        //private void teamA_OnClick(object sender, RoutedEventArgs e)
        //{
        //    if (string.IsNullOrEmpty(teamA.Text)) return;
        //    string js = $"ChangeName('TeamAName','{teamA.Text}')";
        //    browser.WebBrowser.ExecuteScriptAsync(js);
        //}

        //private void teamB_OnClick(object sender, RoutedEventArgs e)
        //{
        //    if (string.IsNullOrEmpty(teamB.Text)) return;
        //    string js = $"ChangeName('TeamBName','{teamB.Text}')";
        //    browser.WebBrowser.ExecuteScriptAsync(js);
        //}

        private void TeamAscore_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"ChangeScore('TeamAscore')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void TeamBscore_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"ChangeScore('TeamBscore')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void Json_Click(object sender, RoutedEventArgs e)
        {
            string json = File.ReadAllText(jsonURL.Text);
            if (string.IsNullOrEmpty(json)) return;
            string js = $"JsonToObj('{json}')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void TeamATimeReset_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"StopTimer('TeamATime')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void TeamATimeStart_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"StartTimer('TeamATime')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void TeamATimeStop_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"StopTimer('TeamATime')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void TeamBTimeStart_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"StartTimer('TeamBTime')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void TeamBTimeStop_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"StopTimer('TeamBTime')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }

        private void TeamBTimeReset_OnClick(object sender, RoutedEventArgs e)
        {
            string js = $"StopTimer('TeamBTime')";
            browser.WebBrowser.ExecuteScriptAsync(js);
        }
    }
}
