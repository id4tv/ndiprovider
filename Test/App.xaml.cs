﻿using System.Windows;
using CefSharp;
using CefSharp.Wpf;

namespace Test
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            var settings = new CefSettings();
            settings.WindowlessRenderingEnabled = false;
            // Increase the log severity so CEF outputs detailed information, useful for debugging
            settings.LogSeverity = LogSeverity.Disable;

            // By default CEF uses an in memory cache, to save cached data e.g. passwords you need to specify a cache path
            // NOTE: The executing user must have sufficent privileges to write to this folder.
            //settings.CachePath = "cache";

            // There are many command line arguments that can either be turned on or off

            // Enable WebRTC
            settings.CefCommandLineArgs.Add("enable-media-stream", "1");

            // Don't use a proxy server, always make direct connections. Overrides any other proxy server flags that are passed.
            // Slightly improves Cef initialize time as it won't attempt to resolve a proxy
            settings.CefCommandLineArgs.Add("no-proxy-server", "1");
            settings.CefCommandLineArgs.Add("transparent-painting-enabled", "1");
            Cef.Initialize(settings);
            base.OnStartup(e);
        }
    }
}
